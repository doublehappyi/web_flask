#coding:utf-8
__author__ = 'db'
from flask import Flask, render_template

app = Flask(__name__)

@app.context_processor
def luck_processor():
    from random import randint
    def lucky_number():
        return randint(1, 10)

    return dict(num=lucky_number())

@app.route('/')
def index():
    return render_template('index.html')

if __name__ =='__main__':
    app.debug = True
    app.run()
