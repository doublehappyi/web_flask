from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/<name>/<int:user_id>')
def hello(name, user_id):
    return "hello %s, user_id:%d" % (name, user_id)

@app.route('/projects/')
def projects():
    return 'projects page'

@app.route('/about')
def about():
    return 'about page'
if __name__ == '__main__':
    app.debug = True
    app.run()
